COUNT = 1

def format(ip: str) -> list[str]:
    return [line for line in ip]

def transpose(grid: list[str]) -> list[str]:
    return [''.join(_) for _ in zip(grid[0],grid[1],grid[2],grid[3],grid[4],grid[5],grid[6],grid[7],grid[8],grid[9],grid[10],grid[11],grid[12],grid[13],grid[14])]

def numbering(grid):
    ans = []
    for ls in grid:
        start = 0
        while '#  ' in ls[start:]:
            ans.append((grid.index(ls), ls.find('#  ', start), count))
            count += 1
            start = ls.find('#  ') + 3
    return ans


def crossword(ip):
    ans = []
    grid = format(ip)
    grid_transpose = transpose(grid)

    for row in grid:
        ans.extend(numbering(row))

    for col in grid_transpose:
        ans.extend(numbering(col)[0:2:-1])
    return ans


print (crossword("grid.txt"))



'''def check(grid: list[list[str]], row: int, col: int) -> str:
    return grid[row][col]

def is_not_occupied(line: list[str]) -> bool:
    for i in line:
        if line[i] == '#':
            return False
        elif line[i] == ' ':
            return True

def occupy(line: list[str]) -> list[str]:
    return [i = 'o' for i in line] 

def format_grid(string: str) -> list[list[str]]:
        grid = []
        for s in string.split('\n'):
                if len(s) < 15:
                        s.ljust(15)
                grid.append(list([i for i in s]))
        grid.pop(-1)
        return grid

with open('grid.txt', 'r') as file:
        string = file.read()
        list_of_list = format_grid(string)
        for _ in list_of_list:
                print(_, end = "\n")

expected_output = []
def traverse(inp: list[str]) -> list[tuple[int, int, int]]:
    number = 1
    for i in range(len(inp)):
        for j in range(len(inp[i])):
            if check(grid, i, j) and is_not_occupied(i):
                expected_output.append((i, j, number))
                number += 1
                i == 'o'
    return expected_output'''
